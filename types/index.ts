export interface City {
  name: string
  state: string
  places?: Place[]
  center?: {
    lat: number
    lng: number
  }
}

export interface Place {
  id: string
  business_id: string
  name: string
  address: string
  city: string
  state: string
  postal_code: string
  latitude: number
  longitude: number
  stars: number
  review_count: number
  is_open?: boolean
  categories: string
}

export interface User {
  chatName: string
  connected: boolean
  firstName?: string
  lastName?: string
  profilePicture?: string
}

export interface Trip {
  id: string
  city: City
  flightDate: Date
  airline: Airline
  places?: Place[]
}

export interface Airline {
  id: number
  name: string
  callsign: string
  country: string
}

export interface ReduxAction {
  type: string
  payload: User | Place
}

export type AppState = {
  trips: {
    [key: string]: Trip
  }
  user?: User | null
  connectedUsers?: User[] | []
  messages: Message[]
  isFetching: boolean
  currentTab: number
  currentCity?: City
  currentExploreMessage?: Message
  sharedPlaces?: Place[]
}

export type Message = {
  body: string
  from: User
}

export type RawMessage = {
  type: string
  origin: string
  message: string
}
