const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: ['./src/components/**/*.{js,ts,jsx,tsx}', './src/pages/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // 'media' or 'class'
  theme: {
    fontFamily: {
      body: ['Nunito', 'Source Sans Pro', 'sans-serif!important'],
      legacy: ['Circular', 'Source Sans Pro', 'sans-serif!important'],
    },
    flex: {
      2: '0 0 50%',
    },
    extend: {
      transformOrigin: {
        // specific for CirclePercentage component.... Do not have a good naming sorry for the confusion..
        sec: '100% 50%',
        fir: '0 50%',
      },
      minHeight: {
        'screen-75': '75vh',
      },
      opacity: {
        80: '.8',
      },
      Index: {
        2: 2,
        3: 3,
      },
      inset: {
        '-100': '-100%',
        '-225-px': '-225px',
        '-160-px': '-160px',
        '-150-px': '-150px',
        '-94-px': '-94px',
        '-50-px': '-50px',
        '-29-px': '-29px',
        '-20-px': '-20px',
        '25-px': '25px',
        '40-px': '40px',
        '95-px': '95px',
        '145-px': '145px',
        '195-px': '195px',
        '210-px': '210px',
        '260-px': '260px',
      },
      height: {
        '95-px': '95px',
        '70-px': '70px',
        '350-px': '350px',
        '500-px': '500px',
        '600-px': '600px',
      },
      maxHeight: {
        '860-px': '860px',
      },
      maxWidth: {
        '100-px': '100px',
        '120-px': '120px',
        '150-px': '150px',
        '180-px': '180px',
        '200-px': '200px',
        '210-px': '210px',
        '580-px': '580px',
        '1/4': '25%',
        '1/3': '33%',
        '1/2': '50%',
        '3/4': '75%',
      },
      minWidth: {
        '140-px': '140px',
        48: '12rem',
      },
      backgroundSize: {
        full: '100%',
      },
      width: {
        'col-two': '49%',
        'col-four': '23%',
        'col-three': '32%',
      },
      colors: {
        dark: {
          900: '#061938',
          800: '#172B4D',
          700: '#253858',
          600: '#344563',
          500: '#42526E',
        },
        mid: {
          400: '#505F79',
          300: '#5E6C84',
          200: '#6B778C',
          100: '#7A869A',
          90: '#8993A4',
          80: '#97A0AF',
          70: '#A5ADBA',
          60: '#B3BAC5',
        },
        light: {
          50: '#C1C7D0',
          40: '#DFE1E6',
          30: '#EBECF0',
          20: '#F4F5F7',
          10: '#FAFBFC',
          0: '#FFFFFF',
        },
        red: {
          500: '#B92500',
          400: '#DE350B',
          300: '#FF5630',
          200: '#FF7452',
          100: '#FF8F73',
          75: '#FFBDAD',
          50: '#FFEBE6',
        },
        yellow: {
          500: '#FF8B00',
          400: '#FF991F',
          350: '#FFA000',
          300: '#FFAB00',
          200: '#FFC400',
          100: '#FFE380',
          75: '#FFF0B3',
          50: '#FFFAE6',
        },
        green: {
          500: '#006644',
          400: '#00875A',
          300: '#36B37E',
          200: '#57D9A3',
          100: '#79F2C0',
          75: '#ABF5D1',
          50: '#E3FCEF',
        },
        blue: {
          500: '#050035',
          400: '#060042',
          300: '#070052',
          200: '#393375',
          100: '#514D86',
          75: '#6A6697',
          50: '#8380A9',
        },
        bluegray: {
          500: '#424067',
          400: '#525081',
          300: '#6764A1',
          200: '#8583B4',
          100: '#9593BD',
          75: '#A4A2C7',
          50: '#B3B2D0',
          25: '#E6E8FF',
        },
        lilac: {
          500: '#564067',
          400: '#6B5081',
          300: '#8664A1',
          200: '#9E83B4',
          100: '#AA93BD',
          75: '#B6A2C7',
          50: '#C3B2D0',
        },
        purplegray: {
          100: '#D8D8F4',
          75: '#F3EFF6',
          50: '#E6E8FF',
        },
        lightred: {
          50: '#FFE5E5',
          100: '#F9E9EE',
          200: '#E23668',
        },
        amber: {
          50: '#FFECB3',
        },
      },
      listStyleType: {
        circle: 'circle',
        square: 'square',
      },
      fontSize: {
        xxs: ['10px', '12px'],
        55: '55rem',
      },
      rotate: {
        '-75': '-75deg',
      },
    },
  },
  variants: ['responsive', 'hover'],
  plugins: [
    require('@tailwindcss/forms'),
    plugin(function ({ addComponents, theme }) {
      const screens = theme('screens', {})
      addComponents([
        {
          '.container': { width: '100%' },
        },
        {
          [`@media (min-width: ${screens.sm})`]: {
            '.container': {
              'max-width': '640px',
            },
          },
        },
        {
          [`@media (min-width: ${screens.md})`]: {
            '.container': {
              'max-width': '768px',
            },
          },
        },
        {
          [`@media (min-width: ${screens.lg})`]: {
            '.container': {
              'max-width': '1024px',
            },
          },
        },
        {
          [`@media (min-width: ${screens.xl})`]: {
            '.container': {
              'max-width': '1280px',
            },
          },
        },
        {
          [`@media (min-width: ${screens['2xl']})`]: {
            '.container': {
              'max-width': '1280px',
            },
          },
        },
      ])
    }),
  ],
}
