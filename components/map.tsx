import { useState } from 'react'
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import { Place } from '../types'
import StarIcon from '@mui/icons-material/Star'
import StarOutlineIcon from '@mui/icons-material/StarOutline'
import SearchIcon from '@mui/icons-material/Search'
import ShareIcon from '@mui/icons-material/Send'
import { useAppSelector } from '../redux/hooks'
import { sharePlaceToChat } from '../redux/actions'
import { useDispatch } from 'react-redux'

type MapProps = {
  center?: { lat: number; lng: number }
  places?: Place[]
}

const MarkerIcon = new L.Icon({
  iconUrl: '/images/marker-icon.svg',
  iconRetinaUrl: '/images/marker-icon.svg',
  // iconAnchor: null,
  // popupAnchor: null,
  // shadowUrl: null,
  // shadowSize: null,
  // shadowAnchor: null,
  iconSize: new L.Point(30, 30),
  className: 'opacity-60',
})

function getStars(stars: number) {
  const starsComp = []
  for (let i = 0; i < 5; i++) {
    if (i < stars) starsComp.push(<StarIcon />)
    else starsComp.push(<StarOutlineIcon />)
  }
  return starsComp
}

const Map = ({ center, places = [] }: MapProps) => {
  const [placeSearchString, setPlaceSearchString] = useState('')
  const { sharedPlaces, user } = useAppSelector((state) => state.app)

  function filterPlaces() {
    if (!placeSearchString) return places
    const s = placeSearchString.toLowerCase()
    return places.filter((p) => {
      return p.categories.toLowerCase().includes(s) || p.name.toLowerCase().includes(s)
    })
  }

  function isPlaceShared(place: Place) {
    console.log(sharedPlaces)
    return sharedPlaces?.map((p) => p.id).includes(place.id)
  }

  const dispatch = useDispatch()
  console.log('rendering')

  if (center) {
    return (
      <div className="w-full h-full overflow-scroll">
        <div className="flex flex-row items-center rounded-lg border-2 text-gray-400 w-full p-1 z-50 bg-white my-1">
          <SearchIcon />
          <input
            className="w-full h-full outline-none"
            placeholder="Search places"
            value={placeSearchString}
            onChange={(e) => setPlaceSearchString(e.target.value)}
          />
        </div>
        <div style={{ height: '90%' }}>
          <MapContainer
            center={[center.lat, center.lng]}
            zoom={13}
            scrollWheelZoom={false}
            style={{ height: '100%', width: '100%' }}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {filterPlaces().map((place, index) => {
              return (
                <Marker
                  key={place.id}
                  position={[place.latitude, place.longitude]}
                  icon={MarkerIcon}
                >
                  <Popup>
                    <div className="flex flex-col text-xs z-50">
                      {place.name}
                      <div className="flex-row items-start justify-items-start text-xs text-yellow-200">
                        {getStars(place.stars)}
                      </div>
                      <span className="text-gray-500">{place.categories}</span>

                      {!isPlaceShared(place) && (
                        <button
                          className="flex flex-row items-center justify-items-center border rounded-lg text-xxs border-gray-300 w-max p-1 text-gray-500"
                          onClick={() => {
                            if (user) dispatch(sharePlaceToChat(place, user))
                            else alert('Connect to chat first!')
                          }}
                        >
                          <ShareIcon fontSize="small" className="text-xs mr-1" />
                          Share to Chat
                        </button>
                      )}
                      {isPlaceShared(place) && <p className="text-xs mr-1">Shared to chat!</p>}
                    </div>
                  </Popup>
                </Marker>
              )
            })}
          </MapContainer>
        </div>
      </div>
    )
  }
  return null
}

export default Map
