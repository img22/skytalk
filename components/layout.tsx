import Head from 'next/head'
import Script from 'next/script'
import { Container } from '@mui/material'
import BottomNav from './nav/bottom'

export default function Layout({ children }: { children: JSX.Element }) {
  return (
    <>
      <Head>
        {/* <link
          rel="stylesheet"
          href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossOrigin=""
        /> */}
      </Head>
      {/* <Script
        src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossOrigin=""
      ></Script> */}
      <Script src="/owt.js"></Script>
      <div style={{ height: '100%', width: '100%' }}>
        <Container sx={{ height: 'calc(100% - 50px)', width: '100%', padding: 1 }}>
          {children}
        </Container>
        <BottomNav />
      </div>
    </>
  )
}
