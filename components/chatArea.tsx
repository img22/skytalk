import type { User, Message } from '../types'
import PersonIcon from '@mui/icons-material/PersonOutline'
import TextField from '@mui/material/TextField'
// import SendIcon from '@mui/icons-material/Send'
import { useState } from 'react'
import { useAppSelector } from '../redux/hooks'

declare type Props = {
  onSendMessage: (body: string) => void
}
export default function ChatArea({ onSendMessage }: Props) {
  const [newMsg, setNewMsg] = useState<string>('')
  const { user, connectedUsers, messages } = useAppSelector((state) => state.app)
  return (
    <div className="flex flex-row h-full w-full">
      <div className="w-max h-full border border-gray-300 bg-white rounded-lg m-0.5">
        {connectedUsers?.map((u) => (
          <div
            key={u.chatName}
            className="flex flex-col text-xxs items-center justify-items-center bg-white p-1 border-b rounded-lg"
          >
            <PersonIcon style={u.chatName === user?.chatName ? { fontStyle: 'bold' } : {}} />
            <span>{u.chatName}</span>
            {u.chatName === user?.chatName && <span>(You)</span>}
          </div>
        ))}
      </div>
      <div className="flex flex-col h-full w-full">
        <div className="flex-grow items-start text-xs p-1 border border-gray-300 bg-white rounded-lg m-0.5">
          {messages.map((message, ind) => {
            return (
              <div key={`m-${ind}`}>
                <span>
                  <b>{message.from.chatName}:&nbsp;</b>
                </span>
                <span>{message.body}</span>
              </div>
            )
          })}
        </div>
        <div className=" w-full">
          <TextField
            label=""
            size="small"
            className="w-full"
            placeholder="Say something..."
            value={newMsg}
            onChange={(e) => setNewMsg(e.target.value)}
            onKeyPress={(keyEvent) => {
              if (keyEvent.key === 'Enter') {
                onSendMessage(newMsg)
                setNewMsg('')
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}
