import * as React from 'react'
import { useRouter } from 'next/router'

import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import BottomNavigation from '@mui/material/BottomNavigation'
import BottomNavigationAction from '@mui/material/BottomNavigationAction'
import HomeIcon from '@mui/icons-material/Home'
import ChatIcon from '@mui/icons-material/Chat'
import ExploreIcon from '@mui/icons-material/Map'
import { useAppSelector } from '../../redux/hooks'
import { useDispatch } from 'react-redux'
import { changeTab } from '../../redux/actions'

export default function BottomNav() {
  const { currentTab } = useAppSelector((state) => state.app)
  const dispatch = useDispatch()
  const router = useRouter()

  const urls = ['/', '/chat', '/explore']

  return (
    <Box className="z-50">
      <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
        <BottomNavigation
          sx={{ height: 50 }}
          showLabels
          value={currentTab}
          onChange={(event, newValue) => {
            if (newValue !== currentTab) {
              dispatch(changeTab(newValue))
              router.push(urls[newValue], undefined, { shallow: true })
            }
          }}
        >
          <BottomNavigationAction label="Home" icon={<HomeIcon />} />
          <BottomNavigationAction label="Chat" icon={<ChatIcon />} />
          <BottomNavigationAction label="Explore" icon={<ExploreIcon />} />
        </BottomNavigation>
      </Paper>
    </Box>
  )
}
