import * as React from 'react'
import Link from 'next/link'
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import CardActions from '@mui/material/CardActions'
import CardContent from '@mui/material/CardContent'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'

import { Trip } from '../types'
import { changeTab, downloadPlaces } from '../redux/actions'
import { useDispatch } from 'react-redux'
import { useAppSelector } from '../redux/hooks'
import { useRouter } from 'next/router'

declare type Props = {
  trip: Trip
  showChat?: boolean
  showDownload?: boolean
  showExplore?: boolean
}

export default function TripCard({
  trip,
  showChat = true,
  showDownload = true,
  showExplore = true,
}: Props) {
  const dispatch = useDispatch()
  const router = useRouter()
  const { isFetching, currentCity } = useAppSelector((state) => state.app)
  return (
    <Box sx={{ minWidth: 275 }} className="mt-2">
      <Card variant="outlined">
        <React.Fragment>
          <CardContent>
            <Typography variant="h5" component="div">
              {trip.city.name}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {trip.airline?.name}
            </Typography>
            <Typography variant="body2">{trip.flightDate.toDateString()}</Typography>
          </CardContent>
          <CardActions>
            {showChat && (
              <Button
                size="small"
                disabled={!currentCity || !currentCity.places}
                onClick={() => {
                  dispatch(changeTab(1))
                  router.push(`/chat/${trip.id}`, undefined, { shallow: true })
                }}
              >
                Chat
              </Button>
            )}
            {showExplore && (
              <Button
                size="small"
                disabled={!currentCity || !currentCity.places}
                onClick={() => {
                  dispatch(changeTab(2))
                  router.push(`/explore/${trip.id}`, undefined, { shallow: true })
                }}
              >
                Explore
              </Button>
            )}
            {showDownload && (
              <Button size="small" onClick={() => dispatch(downloadPlaces(trip))}>
                {isFetching ? 'Downloading...' : 'Download'}
              </Button>
            )}
          </CardActions>
        </React.Fragment>
      </Card>
    </Box>
  )
}
