// const withOffline = require('next-offline')
// const nextConfig = {
//   target: 'serverless',
//   transformManifest: manifest => ['/'].concat(manifest), // add the homepage to the cache
//   // Trying to set NODE_ENV=production when running yarn dev causes a build-time error so we
//   // turn on the SW in dev mode so that we can actually test it
//   generateInDevMode: true,
//   workboxOpts: {
//     swDest: 'static/service-worker.js',
//     runtimeCaching: [
//       {
//         urlPattern: /^https?.*/,
//         handler: 'NetworkFirst',
//         options: {
//           cacheName: 'https-calls',
//           networkTimeoutSeconds: 15,
//           expiration: {
//             maxEntries: 150,
//             maxAgeSeconds: 30 * 24 * 60 * 60, // 1 month
//           },
//           cacheableResponse: {
//             statuses: [0, 200],
//           },
//         },
//       },
//     ],
//   },
//   publicRuntimeConfig: {
//     isDev: true,
//     BACKEND_API: 'http://localhost:5000/'
//   },
// }
const HOST = 'localhost' //'10.27.71.69'
const nextConfig = {
  publicRuntimeConfig: {
    BACKEND_API: `http://${HOST}:5000/`,
    INTEL_SOCKET_API: `http://${HOST}:8095/`
  },
}
module.exports = nextConfig
