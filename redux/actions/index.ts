import { City, Message, Place, Trip, User } from '../../types'
import { appSlice } from '../reducers/app'
import type { AppGlobalState } from '../store'
import { getBusinesses, getCityCenter } from '../../data/Api'

export const setCurrentUser = (user: User) => (dispatch) => {
  console.log('set user', user)
  dispatch(appSlice.actions.setUser(user))
}

export const newTrip = (trip: Trip) => (dispatch, getState: () => AppGlobalState) => {
  const { app } = getState()
  const currentTrips = app.trips
  dispatch(appSlice.actions.setTrips({ ...currentTrips, [trip.id]: trip }))
}

export const addConnectedUser = (user: User) => (dispatch, getState: () => AppGlobalState) => {
  const {
    app: { connectedUsers },
  } = getState()
  if (connectedUsers && !connectedUsers.find((u) => u.chatName === user.chatName))
    dispatch(appSlice.actions.setConnectedUsers([...connectedUsers, user]))
  else dispatch(appSlice.actions.setConnectedUsers([user]))
}

export const addMessage =
  (fromUsername: string, body: string) => (dispatch, getState: () => AppGlobalState) => {
    const {
      app: { messages, connectedUsers },
    } = getState()

    const from = connectedUsers?.find((u) => u.chatName === fromUsername)

    if (from) {
      const message = { from, body }
      dispatch(appSlice.actions.setMessages([...messages, message]))
    }
  }

export const downloadPlaces = (trip: Trip) => async (dispatch, getState: () => AppGlobalState) => {
  dispatch(appSlice.actions.setIsFetching(true))
  const data = await getBusinesses(trip.city)
  if (data !== null) {
    dispatch(appSlice.actions.setCurrentCity(trip.city))
    dispatch(
      appSlice.actions.setCurrentCityCenter({
        lat: parseFloat(data.city.lat),
        lng: parseFloat(data.city.lng),
      })
    )
    dispatch(appSlice.actions.setCurrentCityPlaces(data.businesses))
  }
  dispatch(appSlice.actions.setIsFetching(false))
}

export const changeTab = (tab: number) => (dispatch) => {
  dispatch(appSlice.actions.setCurrentTab(tab))
}

export const sharePlaceToChat = (place: Place, currentUser?: User) => (dispatch) => {
  const msg = {
    body: `${place.name} rated ${place.stars}`,
    from: currentUser,
  } as Message
  dispatch(appSlice.actions.setCurrentExploreMessage(msg))
  dispatch(appSlice.actions.addSharedPlace(place))
}
