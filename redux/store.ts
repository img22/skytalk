// From https://github.com/kirill-konshin/next-redux-wrapper/blob/master/packages/demo-redux-toolkit/store.tsx
import { configureStore } from '@reduxjs/toolkit'
import { appReducer } from './reducers'

export function makeStore() {
  return configureStore({
    reducer: { app: appReducer },
  })
}

export type AppStore = ReturnType<typeof makeStore>
export type AppGlobalState = ReturnType<AppStore['getState']>
