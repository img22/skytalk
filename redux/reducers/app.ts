import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { AppState, Message } from '../../types'

const initialState: AppState = {
  trips: {},
  user: null,
  connectedUsers: [],
  messages: [],
  isFetching: false,
  currentTab: 0,
  currentCity: undefined,
  currentExploreMessage: undefined,
  sharedPlaces: [],
}

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setUser: (state, action) => {
      return {
        ...state,
        user: action.payload,
      }
    },
    setConnectedUsers: (state, action) => {
      return {
        ...state,
        connectedUsers: action.payload,
      }
    },
    setTrips: (state, action) => {
      return {
        ...state,
        trips: action.payload,
      }
    },
    setMessages: (state, action) => {
      return {
        ...state,
        messages: action.payload,
      }
    },
    setTrip: (state, action) => {
      return {
        ...state,
        trips: {
          ...state.trips,
          [action.payload.id]: action.payload,
        },
      }
    },
    setIsFetching: (state, action) => {
      return {
        ...state,
        isFetching: action.payload,
      }
    },
    setCurrentTab: (state, action) => {
      state.currentTab = action.payload
    },
    setCurrentCity: (state, action) => {
      state.currentCity = action.payload
    },
    setCurrentCityPlaces: (state, action) => {
      if (state.currentCity) state.currentCity.places = action.payload
    },
    setCurrentCityCenter: (state, action) => {
      if (state.currentCity) state.currentCity.center = action.payload
    },
    setCurrentExploreMessage: (state, action: PayloadAction<Message>) => {
      state.currentExploreMessage = action.payload
    },
    addSharedPlace: (state, action) => {
      state.sharedPlaces?.push(action.payload)
    },
  },
})

export default appSlice.reducer
