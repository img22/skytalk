import cities from './cities.json'
import airlines from './airlines.json'
import states from './states.json'
import type { Airline } from '../types'

declare type SKCities = {
  [key: string]: string[]
}

// export function getCities() {
//   const destinations = cities as SKCities
//   return Object.keys(destinations)
//     .map((state: string) =>
//       destinations[state].map((c: string) => {
//         const abbr = states.find((s) => s.name === state)
//         return { name: c, state: abbr?.abbreviation }
//       })
//     )
//     .flat()
// }

export function getAirlines() {
  return airlines as Airline[]
}
