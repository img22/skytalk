import axios, { AxiosResponse, Method } from 'axios'
import getConfig from 'next/config'
import camelCaseKeys from 'camelcase-keys'

import { City, Place } from '../types'

const {
  publicRuntimeConfig: { BACKEND_API },
} = getConfig()
console.log(BACKEND_API)
const fetchInstance = axios.create({
  baseURL: BACKEND_API,
  timeout: 30000,
})

type APIFetchArgs = {
  path: string
  method?: Method
  params?: any
  headers?: any
  authToken?: string
  data?: any
}

export const apiFetch = async ({
  path,
  method = 'get',
  params = {},
  data = {},
  headers = {},
  authToken = null,
}: APIFetchArgs) => {
  if (authToken !== null) {
    headers = {
      ...headers,
      Authorization: `token ${authToken}`,
      'x-openigloo-origin': 'web',
    }
  }
  const response = (await fetchInstance.request({
    method,
    url: path,
    params,
    headers,
    data,
  })) as AxiosResponse<any>
  if (!response.data) {
    throw new Error('No response from API')
  }

  return camelCaseKeys(response.data, { deep: true })
}

export const getCityCenter = async (city: City) => {
  try {
    const data = await apiFetch({
      path: `/?city=${city.name}&country=US`,
    })
    return data
  } catch (err) {
    console.error(err)
    return null
  }
}

export const getBusinesses = async (
  city: City
): Promise<{ businesses: Place[]; city: { lat: string; lng: string } } | null> => {
  try {
    const data = await apiFetch({
      path: `/?city=${city.name}&state=${city.state}`,
    })
    return data
  } catch (err) {
    console.error(err)
    return null
  }
}

export const getCityOptions = async () => {
  try {
    const data = await apiFetch({
      path: '/cities',
    })
    return data.cities
  } catch (err) {
    console.error(err)
    return null
  }
}
