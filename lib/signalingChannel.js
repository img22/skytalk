import {io} from 'socket.io-client'
// Copyright (C) <2018> Intel Corporation
//
// SPDX-License-Identifier: Apache-2.0

'use strict';

const wsServer = null
// const allowedRemoteIds = []


/**
 * @class SignalingChannel
 * @classDesc Signaling module for Open WebRTC Toolkit P2P chat
 */
 const signalingChannel = {
  onMessage: null,
  allowedRemoteIds: [],
  wsServer,
  send: function(targetId, message) {
    const data = {
      data: message,
      to: targetId,
    };
    return new Promise((resolve, reject) => {
      wsServer.emit('owt-message', data, function(err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  },

  connect: (loginInfo) => {
    const MAX_TRIALS = 10;
    let reconnectTimes = 0;
    const serverAddress = loginInfo.host;
    let connectPromise = null;

    const opts = {
      'reconnection': true,
      'reconnectionAttempts': MAX_TRIALS,
      'force new connection': true,
    };
    wsServer = io(serverAddress, opts);

    wsServer.on('connect', function() {
      reconnectTimes = 0;
      wsServer.emit('authentication', {token: loginInfo.token}, (data) => {
        if (data.uid) {
          console.log('Authentication passed. User ID: ' + data.uid);
        } else {
          console.error('Faild to connect to Socket.IO server.');
        }
        if (connectPromise) {
          if (data.uid) {
            connectPromise.resolve(data.uid);
          } else if (data.error) {
            connectPromise.reject(data.error);
          }
        }
        connectPromise = null;
      });
    });

    wsServer.on('reconnecting', function() {
      reconnectTimes++;
    });

    wsServer.on('reconnect_failed', function() {
      if (signalingChannel.onServerDisconnected) {
        signalingChannel.onServerDisconnected();
      }
    });

    wsServer.on('server-disconnect', function() {
      reconnectTimes = MAX_TRIALS;
    });

    wsServer.on('disconnect', function() {
      console.info('Disconnected from websocket server.');
      if (reconnectTimes >= MAX_TRIALS && signalingChannel.onServerDisconnected) {
        signalingChannel.onServerDisconnected();
      }
    });

    wsServer.on('connect_failed', function(errorCode) {
      console.error('Connect to websocket server failed, error:' +
        errorCode + '.');
      if (connectPromise) {
        connectPromise.reject(parseInt(errorCode));
      }
      connectPromise = null;
    });

    wsServer.on('error', function(err) {
      console.error('Socket.IO error:' + err);
      if (err == '2103' && connectPromise) {
        connectPromise.reject(err);
        connectPromise = null;
      }
    });

    wsServer.on('owt-message', function(data) {
      console.info('Received owt message.');
      if (signalingChannel.onMessage) {
        signalingChannel.onMessage(data.from, data.data);
      }
    });

    wsServer.on('new-user', (data) => {
        console.log('got new user', signalingChannel.allowedRemoteIds)
        signalingChannel.allowedRemoteIds.push(data.uid)

        if(signalingChannel.chatClient) {
            signalingChannel.chatClient.allowedRemoteIds = signalingChannel.allowedRemoteIds
        }

        if(signalingChannel.onOthersJoined) {
            signalingChannel.onOthersJoined(data.uid)
        }
    })

    return new Promise((resolve, reject) => {
      connectPromise = {
        resolve,
        reject,
      };
    });
  },

  disconnect: function() {
    reconnectTimes = MAX_TRIALS;
    if (wsServer) {
      wsServer.close();
    }
    return Promise.resolve();
  },
  chatClient: null,
  onOthersJoined: (newUser) => {}
}

export default function SignalingChannel() {
    return signalingChannel
} 