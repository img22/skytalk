import { ReactElement, useState } from 'react'
import { styled } from '@mui/material/styles'
import AddIcon from '@mui/icons-material/Add'
import {
  Dialog,
  DialogTitle,
  Button,
  Paper,
  DialogActions,
  DialogContent,
  TextField,
  Autocomplete,
} from '@mui/material'
import DatePicker from '@mui/lab/DatePicker'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import LocalizationProvider from '@mui/lab/LocalizationProvider'

import Layout from '../../components/layout'
import TripCard from '../../components/tripCard'

import { useAppSelector } from '../../redux/hooks'
import { useDispatch } from 'react-redux'

export default function Page() {
  const { trips } = useAppSelector((state) => state.app)

  return (
    <div>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <div style={{ textAlign: 'center', margin: 'auto 0px', width: 120 }}>
          <h3 style={{ margin: 0 }}>Select Trip</h3>
        </div>
      </div>
      {trips &&
        Object.values(trips).map((trip) => {
          return (
            <TripCard
              trip={trip}
              key={trip.city.name}
              showChat={true}
              showDownload={false}
              showExplore={false}
            />
          )
        })}
    </div>
  )
}

Page.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>
}
