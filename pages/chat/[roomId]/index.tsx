import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import getConfig from 'next/config'
import { Button } from '@mui/material'
import RadarIcon from '@mui/icons-material/Radar'
import { uniqueNamesGenerator, adjectives, animals } from 'unique-names-generator'
import { useDispatch } from 'react-redux'

import Layout from '../../../components/layout'
import ChatArea from '../../../components/chatArea'
import SignalingChannel from '../../../lib/signalingChannel'
import { useAppSelector } from '../../../redux/hooks'
import { setCurrentUser, addConnectedUser, addMessage, changeTab } from '../../../redux/actions'
import { Message, User, RawMessage } from '../../../types'

const {
  publicRuntimeConfig: { INTEL_SOCKET_API },
} = getConfig()

export default function Chat() {
  // const [signalingChannel, setSignalingChannel] = useState<any>(SignalingChannel())
  const [chatClient, setChatClient] = useState<any>(null)
  const { user, connectedUsers, currentExploreMessage } = useAppSelector((state) => state.app)
  const dispatch = useDispatch()
  const signalingChannel = SignalingChannel()
  const router = useRouter()

  function getUniqueName() {
    return uniqueNamesGenerator({
      dictionaries: [adjectives, animals],
    })
  }

  signalingChannel.onOthersJoined = (newUserName: string) => {
    if (chatClient) {
      console.log('New user joined', newUserName)
      const newUser = { chatName: newUserName } as User
      dispatch(addConnectedUser(newUser))
      if (chatClient.allowedRemoteIds) {
        chatClient.allowedRemoteIds.push(newUserName)
      }
    }
  }

  const onMessage = (raw: RawMessage) => {
    const fromUsername = raw.origin
    const body = JSON.parse(raw.message).data
    dispatch(addMessage(fromUsername, body))
  }

  async function onSendMessage(body: string) {
    if (user) {
      dispatch(addMessage(user?.chatName, body))
      const otherUserNames = connectedUsers
        ? connectedUsers
            .filter((u) => u && user && u.chatName !== user.chatName)
            .map((u) => u.chatName)
        : []

      if (chatClient) {
        const promises = otherUserNames.map((uname: string) => {
          console.log('sending to', uname)
          return chatClient.send(uname, JSON.stringify({ data: body }))
        })
        await Promise.all(promises)
      }
    }
  }

  const chatConnect = () => {
    if (window !== undefined) {
      //-- Import Intel WebRTC Library
      const Owt = require('owt-client-javascript/dist/sdk/owt.js')
      const serverAddress = INTEL_SOCKET_API
      const chatName = getUniqueName()

      const client = new Owt.P2P.P2PClient(
        {
          audioEncodings: false,
          videoEncodings: false,
          rtcConfiguration: { iceServers: [] },
        },
        signalingChannel
      )
      client.connect({ host: serverAddress, token: chatName }).then(() => {
        dispatch(setCurrentUser({ chatName, connected: true }))
        dispatch(addConnectedUser({ chatName, connected: true }))
      })
      client.addEventListener('messagereceived', onMessage)

      setChatClient(client)
      signalingChannel.chatClient = client
    }
  }

  // useEffect(() => {
  //   const path = router.pathname
  //   if (path.startsWith('/chat')) {
  //     dispatch(changeTab(1))
  //   } else if (path.startsWith('/explore')) {
  //     dispatch(changeTab(2))
  //   }
  // }, [])

  useEffect(() => {
    if (user && user.connected && currentExploreMessage) onSendMessage(currentExploreMessage.body)
  }, [currentExploreMessage])

  return (
    <Layout>
      <div className="flex flex-col h-full w-full justify-center items-center">
        {!user && (
          <Button variant="text" startIcon={<RadarIcon />} size="small" onClick={chatConnect}>
            Join Nearby
          </Button>
        )}
        {user && user.connected && connectedUsers && <ChatArea onSendMessage={onSendMessage} />}
      </div>
    </Layout>
  )
}
