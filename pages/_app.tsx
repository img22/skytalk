import type { ReactElement, ReactNode } from 'react'
import type { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { makeStore } from '../redux/store'
import '../styles/globals.css'
import '../styles/global.sass' //IMPORTANT: this needs to be the last style file

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

const store = makeStore()

function SkyTalkApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)
  return <Provider store={store}>{getLayout(<Component {...pageProps} />)}</Provider>
}

export default SkyTalkApp
