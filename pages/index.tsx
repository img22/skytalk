import { ReactElement, useEffect, useState } from 'react'
import { styled } from '@mui/material/styles'
import AddIcon from '@mui/icons-material/Add'
import {
  Dialog,
  DialogTitle,
  Button,
  Paper,
  DialogActions,
  DialogContent,
  TextField,
  Autocomplete,
} from '@mui/material'
import DatePicker from '@mui/lab/DatePicker'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import LocalizationProvider from '@mui/lab/LocalizationProvider'

import Layout from '../components/layout'
import TripCard from '../components/tripCard'

import { getAirlines } from '../data'
import { getCityOptions } from '../data/Api'
import { useAppSelector } from '../redux/hooks'
import { newTrip } from '../redux/actions'
import { useDispatch } from 'react-redux'
import { Airline, City } from '../types'

const getAirlineOptions = () => {
  const uniqueOptions: Airline[] = []
  getAirlines().forEach((a) => {
    if (!uniqueOptions.map((o) => o.name).includes(a.name)) {
      uniqueOptions.push(a)
    }
  })
  // return getAirlines().map((l) => )
  return uniqueOptions.map((l) => ({ label: l.name, value: l, id: l.id }))
}

export default function Page() {
  const [showNewTripDialog, setShowNewTripDialog] = useState<boolean>(false)
  const [cityOptions, setCityOptions] = useState()

  const [city, setCity] = useState<City>({} as City)
  const [tripDate, setTripDate] = useState<Date>(new Date())
  const [airline, setAirline] = useState<Airline>({} as Airline)
  const [loading, setLoading] = useState<boolean>(false)

  const { trips } = useAppSelector((state) => state.app)
  const dispatch = useDispatch()

  const cities = getCityOptions()
  const airlines = getAirlineOptions()

  async function fetchCityOptions() {
    setLoading(true)
    const cities = await getCityOptions()
    const opts = cities.map((d: { city: string; state: string }) => ({
      label: `${d.city}, ${d.state}`,
      value: { name: d.city, state: d.state } as City,
    }))
    setCityOptions(opts)
    setLoading(false)
  }

  useEffect(() => {
    fetchCityOptions()
  }, [])

  const newTripDialog = (
    <Dialog
      open={showNewTripDialog}
      onClose={() => setShowNewTripDialog(false)}
      maxWidth="lg"
      fullWidth={true}
    >
      <br />
      <DialogTitle>Add a new trip</DialogTitle>
      <DialogContent>
        <Autocomplete
          disablePortal
          options={cityOptions}
          renderInput={(params) => <TextField {...params} label="Select city" />}
          sx={{ marginTop: 1, marginBottom: 5 }}
          onChange={(_, newVal) => {
            newVal ? setCity(newVal.value) : null
          }}
        />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DatePicker
            label="Basic example"
            value={tripDate}
            onChange={(newValue: Date | null) => {
              setTripDate(newValue ? newValue : new Date())
            }}
            renderInput={(params) => <TextField {...params} label="Select your trip date" />}
          />
        </LocalizationProvider>
        <Autocomplete
          disablePortal
          options={airlines}
          renderInput={(params) => <TextField {...params} label="Select airline" />}
          sx={{ marginTop: 1, marginBottom: 5 }}
          onChange={(_, newVal) => {
            newVal ? setAirline(newVal.value) : null
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setShowNewTripDialog(false)
          }}
        >
          Cancel
        </Button>
        <Button
          onClick={() => {
            setShowNewTripDialog(false)
            dispatch(
              newTrip({
                id: `${city.name}-${city.state}-${tripDate.toDateString()}-${airline.callsign}`,
                city,
                flightDate: tripDate,
                airline,
              })
            )
          }}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  )
  if (loading) {
    return <p>Loading...</p>
  }
  return (
    <div>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <div style={{ textAlign: 'center', margin: 'auto 0px', width: 120 }}>
          <h3 style={{ margin: 0 }}>Your Trips</h3>
        </div>
        <div style={{ textAlign: 'center', margin: 'auto 0px', width: 120 }}>
          <Button
            variant="text"
            startIcon={<AddIcon />}
            size="small"
            onClick={() => setShowNewTripDialog(!showNewTripDialog)}
          >
            New Trip
          </Button>
        </div>
      </div>
      {newTripDialog}
      {trips &&
        Object.values(trips).map((trip) => {
          return <TripCard trip={trip} key={trip.city.name} />
        })}
    </div>
  )
}

Page.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>
}
