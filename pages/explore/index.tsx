import { ReactElement } from 'react'

import Layout from '../../components/layout'
import TripCard from '../../components/tripCard'

import { useAppSelector } from '../../redux/hooks'

export default function Page() {
  const { trips } = useAppSelector((state) => state.app)

  return (
    <div>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <div style={{ textAlign: 'center', margin: 'auto 0px', width: 120 }}>
          <h3 style={{ margin: 0 }}>Select Trip</h3>
        </div>
      </div>
      {trips &&
        Object.values(trips).map((trip) => {
          return (
            <TripCard
              trip={trip}
              key={trip.city.name}
              showChat={false}
              showDownload={false}
              showExplore={true}
            />
          )
        })}
    </div>
  )
}

Page.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>
}
