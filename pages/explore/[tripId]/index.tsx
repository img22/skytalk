import dynamic from 'next/dynamic'

import Layout from '../../../components/layout'

import { useAppSelector } from '../../../redux/hooks'

const Map = dynamic(() => import('../../../components/map'), { ssr: false })

export default function Explore() {
  const { currentCity, currentTab } = useAppSelector((state) => state.app)
  return (
    <Layout>
      <div className="flex flex-col h-full w-full border-1 justify-center items-center" id="map">
        <Map center={currentCity?.center} places={currentCity.places.slice(0, 100)} />
      </div>
    </Layout>
  )
}
