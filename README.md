SkyTalk client. For chat to work you must run the Intel WebRTC Peer-to-Peer Server along with this client. Found [here](https://github.com/open-webrtc-toolkit/owt-server-p2p)

## Getting Started

1. First setup the [skytalk p2p server](https://gitlab.com/img22/skytalk-p2p-server.git). Follow instructions there and run the server
2. Setup the [skytalk server](https://gitlab.com/img22/skytalk-server.git). Follow instruction to setup the data and run the server
3. Run the following, to run this client app:

```bash
npm i
npm run dev
# or
yarn
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
